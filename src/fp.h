//----------------------------------------------------------------------------------------------------------------------
// Fixed point maths
//----------------------------------------------------------------------------------------------------------------------

#pragma once

const int kScale = 8;
const int kFracMask = 0xffff >> (16 - kScale);
const int kWholeMask = -1 ^ kFracMask;

class fp
{
public:
    fp() : x(0) {}
    fp(f32 f) : x(i16(f * f32(1 << kScale))) {}
    fp(i32 i) : x(i << kScale) {}

    operator i32() const { return x >> kScale; }
    operator f32() const 
    { 
        return f32(x) / f32(1 << kScale); 
    }

    fp operator+ (const fp f) const 
    { 
        return create(x + f.x); 
    }

    fp operator- (const fp f) const 
    { 
        return create(x - f.x); 
    }

    fp operator* (const fp f) const
    {
        i32 m = i32(x) * i32(f.x);
        return create(m >> kScale);

        //return create((x >> 4) * (f.x >> 4));
    }

    fp operator/ (const fp& f) const
    {
        return create((i32(x) << kScale) / f.x);
    }

    bool operator < (const fp f) const { return x < f.x; }
    bool operator > (const fp f) const { return x > f.x; }
    bool operator <= (const fp f) const { return x <= f.x; }
    bool operator >= (const fp f) const { return x >= f.x; }
    bool operator == (const fp f) const { return x == f.x; }
    bool operator != (const fp f) const { return x != f.x; }

    static fp epsilon() { return create(1); }
    
    fp whole() const { return create(x & kWholeMask); }
    fp frac() const { return create(x & kFracMask); }

protected:
    static fp create(i16 i)
    {
        fp f;
        f.x = i;
        return f;
    }

private:
    i16     x;
};

