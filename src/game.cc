//----------------------------------------------------------------------------------------------------------------------
// Game code
//----------------------------------------------------------------------------------------------------------------------

#include "game.h"

#include <functional>
#include <map>
#include <sstream>
#include <random>
#include <fp.h>

//----------------------------------------------------------------------------------------------------------------------
// Globals
//----------------------------------------------------------------------------------------------------------------------

global_variable int gScreen = 1;

//----------------------------------------------------------------------------------------------------------------------
// Utilities
//----------------------------------------------------------------------------------------------------------------------

std::mt19937 gRng;
std::uniform_real_distribution<float> gDist(0, 1);

fp frand()
{
    fp r = gDist(gRng);
    return r;

}

//----------------------------------------------------------------------------------------------------------------------
// Game lifetime
//----------------------------------------------------------------------------------------------------------------------

static const int kNumStars = 256;
static const fp kSpread = 64.0f;
static const fp kSpeed = 20.0f;

static fp gStarX[kNumStars];
static fp gStarY[kNumStars];
static fp gStarZ[kNumStars];


void initStar(int index)
{
    gStarX[index] = fp(2) * (frand() - fp(0.5f)) * kSpread;
    gStarY[index] = fp(2) * (frand() - fp(0.5f)) * kSpread;
    gStarZ[index] = fp(2) * (frand() + fp(0.0001f)) * kSpread;
}


void initGame()
{
    for (int i = 0; i < kNumStars; ++i)
    {
        initStar(i);
    }
}

void doneGame()
{
}

//----------------------------------------------------------------------------------------------------------------------
// Stars update
//----------------------------------------------------------------------------------------------------------------------

void starsUpdateAndRender(GameOffScreenBuffer& buffer, GameInput& input)
{
    int halfWidth = buffer.width / 2;
    int halfHeight = buffer.height / 2;

    for (int i = 0; i < kNumStars; ++i)
    {
//         fp dt = f32(input.dt);
//         fp speed = dt * kSpeed;
        gStarZ[i] = gStarZ[i] - fp::epsilon();
        if (gStarZ[i] <= fp(0))
        {
            initStar(i);
        }

        fp xx = gStarX[i] / gStarZ[i];
        fp yy = gStarY[i] / gStarZ[i];

        int x = (int(f32(xx) * halfWidth) + halfWidth);
        int y = (int(f32(yy) * halfHeight) + halfHeight);

        if (x < 0 || x >= buffer.width ||
            y < 0 || y >= buffer.height)
        {
            initStar(i);
        }
        else
        {
            buffer.memory[y * buffer.width + x] = 0xffffffff;
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
// Update and render game
//----------------------------------------------------------------------------------------------------------------------

void updateAndRenderGame(GameOffScreenBuffer& buffer, GameInput& input)
{
    for (int i = 0; i < (buffer.width * buffer.height); ++i)
    {
        buffer.memory[i] = 0xff000000;
    }

    starsUpdateAndRender(buffer, input);
}

//----------------------------------------------------------------------------------------------------------------------
// Main loop
//----------------------------------------------------------------------------------------------------------------------

void gameUpdateAndRender(GameOffScreenBuffer& buffer, GameInput& input)
{
    auto funcs = std::map<int, std::function<void(GameOffScreenBuffer&, GameInput&)>>{
        { 1, updateAndRenderGame },
    };

    if (funcs.find(input.functionKey) != funcs.end())
    {
        gScreen = input.functionKey;
    }

    funcs.at(gScreen)(buffer, input);
}

